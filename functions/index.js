const functions = require("firebase-functions");
const { WebhookClient, Card, Suggestion } = require("dialogflow-fulfillment");

const express = require("express");
const TelegramBot = require("telegram-bot-api");
const admin = require("firebase-admin");
const multer = require("multer");

admin.initializeApp();

const upload = multer({ storage: multer.memoryStorage() });
const sendPhotoApp = express();
sendPhotoApp.post("/:secret", upload.single("photo"), async (req, res) => {
  const config = functions.config();
  const telegram = TelegramBot(config.telegram_bot_token);

  const secret = req.params.chat;
  const chatRef = await admin.firestore
    .collection("subscribers")
    .where("secret", "==", secret)
    .get();

  chatRef.forEach(chatDoc => {
    const chatId = chat.data().chat_id;
    console.log(
      `sendPhoto: sending photo from ${req.ip} to telegram ${chatId}`
    );
    // https://github.com/yagop/node-telegram-bot-api/blob/release/doc/api.md#TelegramBot+sendPhoto
    telegram.sendPhoto(chatId, req.file.buffer);
  });

  res.sendStatus(200);
  res.end();
});

exports.sendPhoto = functions.https.onRequest(sendPhotoApp);

exports.dialogFlow = functions.https.onRequest(async (request, response) => {
  const agent = new WebhookClient({ request, response });

  /*
    { source: 'telegram',
      payload:
      { data: { update_id: 556414073, message: [Object] },
        source: 'telegram' } } {"source":"telegram","payload":{"data":{"update_id":556414073,"message":{"date":1538342024,"chat":{"id":42963421,"type":"private","first_name":"Denis","username":"denis4inet"},"from":{"language_code":"en-US","id":42963421,"is_bot":false,"first_name":"Denis","username":"denis4inet"},"message_id":45,"text":"Поставь на охрану"}},"source":"telegram"
      }
    }
     */

  const source = agent.originalRequest.payload.source;
  switch (source) {
    case "telegram":
      {
        const chat = agent.originalRequest.payload.data.message.chat;
        await admin.firestore.collections('subsribers').doc().set({secret: '', ...chat, id: undefined})
      }
      break;
    default:
      console.error(`${source} is not supported yet`);
  }
  console.log(
    `dialogFlow: ${agent.requestSource}, ${agent.originalRequest}`,
    agent.originalRequest,
    JSON.stringify(agent.originalRequest)
  );
  response.end();
});
